import calculatePrize from './calculate-prize';

const openSnackbar = () => ({
  type: 'OPEN_SNACKBAR'
});

const closeSnackbar = () => ({
  type: 'CLOSE_SNACKBAR'
});

const setSnackbarMessage = (message) => ({
  type: 'SET_SNACKBAR_MESSAGE',
  message
})

export const addMoney = (amount) => ({
  type: 'ADD_MONEY',
  amount
});

export const removeMoney = (amount) => ({
  type: 'REMOVE_MONEY',
  amount
});

const winMoney = (amount) => (dispatch) => {
  dispatch(addMoney(amount));
  dispatch(setSnackbarMessage('🎉Won '+amount+'!!🎉'))
  dispatch(openSnackbar());

  return new Promise((resolve) => {
    setTimeout(() => {
      dispatch(closeSnackbar());
      resolve();
    }, 1000);
  });
}

export const spin = () => (dispatch, getState) => {
  dispatch(removeMoney(200));
  dispatch({
    type: 'START_SPIN'
  });
  let timer = setInterval(() => {
    dispatch({type: 'SPIN'});
  },100);
  return new Promise((resolve) => {
    setTimeout(() => {
      clearInterval(timer);
      dispatch({type: 'STOP_SPIN'});
      let prize = calculatePrize(getState().slots);
      if(prize > 0) {
        dispatch(winMoney(prize));
      }
      resolve();
    }, 3000);
  });
}

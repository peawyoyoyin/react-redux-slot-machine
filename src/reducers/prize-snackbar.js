import { combineReducers } from 'redux';

const open = (state=false, action) => {
  switch(action.type) {
    case 'OPEN_SNACKBAR':
      return true;
    case 'CLOSE_SNACKBAR':
      return false;
    default:
      return state;
  }
}

const message = (state='no message', action) => {
  switch(action.type) {
    case 'SET_SNACKBAR_MESSAGE':
      return action.message;
    default:
      return state;
  }
}

const prizeSnackbar = combineReducers({
  open,
  message
});

export default prizeSnackbar;

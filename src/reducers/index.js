import { combineReducers } from 'redux';
import money from './money';
import isSpinning from './is-spinning';
import slots from './slots';
import prizeSnackbar from './prize-snackbar';

const mainReducer = combineReducers({
  money,
  isSpinning,
  slots,
  prizeSnackbar
});

export default mainReducer;

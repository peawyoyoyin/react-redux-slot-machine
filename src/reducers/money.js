const money = (state=0, action) => {
  switch(action.type) {
    case 'ADD_MONEY':
      return state + action.amount;
    case 'REMOVE_MONEY':
      return state - action.amount > 0 ? state - action.amount : 0;
    default:
      return state
  }
};

export default money;

const isSpinning = (state=false, action) => {
  switch(action.type) {
    case 'START_SPIN':
      return true;
    case 'STOP_SPIN':
      return false;
    default:
      return state;
  }
}

export default isSpinning;

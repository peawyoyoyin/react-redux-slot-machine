const NUMBER_OF_SLOTS = 8;

const slots = (state = [0,0,0], action) => {
  switch(action.type) {
    case 'SET_SLOT':
      return [...state.slice(0,action.id - 1),
              action.value,
              ...state.slice(action.id)]
    case 'SPIN':
      return state.map( () => Math.floor(Math.random()*NUMBER_OF_SLOTS) );
    default:
      return state;
  }
}

export default slots;

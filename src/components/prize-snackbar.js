import React from 'react';
import Snackbar from 'material-ui/Snackbar';

const PrizeSnackbarUI = ({open, message}) => (
  <Snackbar open={open} message={message} />
);

export default PrizeSnackbarUI;

import React from 'react';
import SlotUI from './slot'
import Paper from 'material-ui/Paper'

const style = {
  margin: 10,
  maxWidth: 390
};

const SlotFaceUI = ({ slots }) => (
  <div>
    < Paper style={style} >
      {
        slots.map((slot, index) => (
          < SlotUI key={index} face={slot} />
        ))
      }
    </ Paper >
  </div>
);

export default SlotFaceUI;

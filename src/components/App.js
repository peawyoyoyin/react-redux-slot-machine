import React, { Component } from 'react';
import SlotFace from '../containers/slot-face';
import SpinButton from '../containers/spin-button';
import MoneyUI from '../containers/money-ui';
import Cheats from './cheats';
import Paper from 'material-ui/Paper';
import PrizeSnackbar from '../containers/prize-snackbar';

const style = {
  padding: 5,
  width: 420,
  backgroundColor: 'gray',
  display: 'block',
  position: 'absolute',
  top: '50%',
  left: '50%',
  marginRight: '-50%',
  transform: 'translate(-50%, -50%)'

};

class App extends Component {
  render() {
    return (
      <div>
      <Paper style={style} zDepth={5}> 
        < SlotFace />
        < SpinButton />
        < MoneyUI />
        < Cheats />
      </Paper>
      < PrizeSnackbar />
      </div>
    );
  }
}

export default App;

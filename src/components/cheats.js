import React from 'react';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import { addMoney, removeMoney } from '../actions'
import { connect } from 'react-redux'

const style = {
  margin: 10
}

const buttonStyle = {
  marginLeft: 5,
  marginRight: 5,
}

let Cheats = ({ dispatch }) => (
  < Paper style={style}>
    < FlatButton style={buttonStyle} onClick={ () =>  {
        dispatch(addMoney(50))
      }
    } 
    rippleColor={'red'} hoverColor={'pink'} >
      Add 50 <span role="img" aria-label="money">💰</span>
    </ FlatButton >
    < FlatButton style={buttonStyle} onClick={ () =>  {
        dispatch(removeMoney(50))
      }
    } 
    rippleColor={'green'} hoverColor={'lightgreen'} >
      Remove 50 <span role="img" aria-label="money">💰</span>
    </ FlatButton >
  </ Paper>
);

Cheats = connect()(Cheats);

export default Cheats;

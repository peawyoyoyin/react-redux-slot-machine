import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
  width: 390,
  height: 36,
  margin: 10
}

const SpinButtonUI = ({ disabled, onSpin }) => (
    < RaisedButton style={style} primary={true} disabled={disabled} onClick={onSpin}>
      Spin
    </ RaisedButton >
);

export default SpinButtonUI;

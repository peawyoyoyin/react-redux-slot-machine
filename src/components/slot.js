import React from 'react';
import Paper from 'material-ui/Paper';

const style = {
  height: 100,
  width: 100,
  margin: 15,
  display: 'inline-block',
  lineHeight: '100px',
  textAlign: 'center',
}

const faceStyle = {
  margin: 0,
  display: 'inline-block',
  verticalAlign: 'middle',
  lineHeight: 'normal',
  fontSize: '3em'
}

const SlotUI = ({ face }) => (
  < Paper style={style} zDepth={2}>
    <h1 style={faceStyle}>
      { face === undefined ? '⛔️' : face }
    </h1>
  </ Paper >
);

export default SlotUI;

import React from 'react';
import Paper from 'material-ui/Paper';

const style = {
  width: 390,
  margin: 10,
  padding: 5,
  textAlign: 'left'
}

const MoneyUIFace = ({ money }) => (
    < Paper style={style}>
      <span role="img" aria-label="money">💰</span>
      <span style={{float: 'right'}}>{ money }</span>
    </ Paper >
);

export default MoneyUIFace;

import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import AppRoot from './components';
import registerServiceWorker from './registerServiceWorker';
import mainReducer from './reducers';
import thunkMiddleware from 'redux-thunk';

injectTapEventPlugin();

let store = createStore(
  mainReducer,  //reducers
  applyMiddleware( //middleware chain
    thunkMiddleware,
    createLogger()
  )
);

ReactDOM.render(<AppRoot store={store} />, document.getElementById('root'));
registerServiceWorker();

import { connect } from 'react-redux';
import SpinButtonUI from '../components/spin-button';
import { spin } from '../actions';

const mapStateToProps = ({ money, isSpinning }) => ({
  disabled: money < 200 || isSpinning
});

const mapDispatchToProps = (dispatch) => ({
  onSpin: () => {
    dispatch(spin());
  }
});

const SpinButton = connect(mapStateToProps, mapDispatchToProps)(SpinButtonUI);
export default SpinButton;

import { connect } from 'react-redux';
import PrizeSnackbarUI from '../components/prize-snackbar'

const mapStateToProps = ({ prizeSnackbar }) => ({
  open: prizeSnackbar.open,
  message: prizeSnackbar.message
});

const PrizeSnackbar = connect(mapStateToProps)(PrizeSnackbarUI);
export default PrizeSnackbar;

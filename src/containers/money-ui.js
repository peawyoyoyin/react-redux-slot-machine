import MoneyUIFace from '../components/money-ui';
import { connect } from 'react-redux';

const mapStateToProps = ({ money }) => ({
  money
});

const MoneyUI = connect(mapStateToProps)(MoneyUIFace);
export default MoneyUI;

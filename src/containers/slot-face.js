import { connect } from 'react-redux';
import SlotFaceUI from '../components/slot-face';
import SlotFaces from '../actions/slot-faces';

const mapStateToProps = ({ slots }) => ({
  slots: slots.map(index => SlotFaces[index])
});

const SlotFace = connect(mapStateToProps)(SlotFaceUI);
export default SlotFace;
